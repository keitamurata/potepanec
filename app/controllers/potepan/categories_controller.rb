class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(:taxons).all
    @taxon      = Spree::Taxon.find(params[:id])
    @products = @taxon.products.includes(master: [:default_price, :images])
  end
end
