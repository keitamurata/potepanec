require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    # subject { get :show, params: { id: product.id } }
    before do
      get :show, params: { id: product.id }
    end

    let(:product) { create(:product) }

    it "レスポンスが正しい" do
      expect(response).to be_successful
    end

    it "正しいテンプレートが表示されている" do
      expect(response).to render_template :show
    end

    it "インスタンス変数が正しくセットされている" do
      expect(assigns(:product)).to eq(product)
    end
  end
end
